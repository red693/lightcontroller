################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Libs/AL_functions.c \
../Libs/DS3231.c \
../Libs/Leds.c \
../Libs/i2c_operations.c \
../Libs/interrupt_handlers.c \
../Libs/main_controller.c \
../Libs/read_but.c 

OBJS += \
./Libs/AL_functions.o \
./Libs/DS3231.o \
./Libs/Leds.o \
./Libs/i2c_operations.o \
./Libs/interrupt_handlers.o \
./Libs/main_controller.o \
./Libs/read_but.o 

C_DEPS += \
./Libs/AL_functions.d \
./Libs/DS3231.d \
./Libs/Leds.d \
./Libs/i2c_operations.d \
./Libs/interrupt_handlers.d \
./Libs/main_controller.d \
./Libs/read_but.d 


# Each subdirectory must supply rules for building sources it contributes
Libs/%.o: ../Libs/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -mfloat-abi=soft -DSTM32F1 -DNUCLEO_F103RB -DSTM32F103RBTx -DSTM32 -DDEBUG -DUSE_STDPERIPH_DRIVER -DSTM32F10X_MD -I"/media/red/Work/SharedFolder/Repos/LightController/inc" -I"/media/red/Work/SharedFolder/Repos/LightController/CMSIS/core" -I"/media/red/Work/SharedFolder/Repos/LightController/CMSIS/device" -I"/media/red/Work/SharedFolder/Repos/LightController/StdPeriph_Driver/inc" -I"/media/red/Work/SharedFolder/Repos/LightController/Utilities/STM32F1xx-Nucleo" -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


