/**
  ******************************************************************************
  * @file    main.c
  * @author  Ac6
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/


#include "stm32f10x.h"
#include "stm32f1xx_nucleo.h"
#include "../Libs/AL_functions.h"
#include "../Libs/local_defines.h"
#include "../Libs/SoftwareTimer.h"
#include "../Libs/Leds.h"
#include "../Libs/main_controller.h"



uint8_t time_out = FALSE;

extern uint8_t received_command;

int main(void)
{

	init_uC();
	for (;;)
	{
		if (time_out == TRUE)
		{
			// Base recurrence at approx 15ms
			adc_task15ms();
			main_control_task15ms();
			time_out = FALSE;
		}
		else if(received_command == TRUE)
		{
			received_command = FALSE;
			interpret_command();
		}
		else
		{
			asm volatile ("nop");
		}
	}
}
