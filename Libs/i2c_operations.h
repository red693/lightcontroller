/*
 * i2c_operations.h
 *
 *  Created on: Jan 12, 2018
 *      Author: red
 */

#ifndef I2C_OPERATIONS_H_
#define I2C_OPERATIONS_H_

#define I2C_OP_LIMIT 4u
#define I2C_SPEED   400000
//#define I2C_SPEED   100000

#define I2C_DMA_CHANNEL_TX DMA1_Channel6
#define I2C_DMA_CHANNEL_RX DMA1_Channel7
#define I2C_DR_Address ((uint32_t)0x40005410)
#define I2C_RX_BUFFER_SIZE 260
#define I2C_TX_BUFFER_SIZE 260

#define I2C_DMA_FLAG_TX_TC      DMA1_IT_TC6
#define I2C_DMA_FLAG_TX_GL      DMA1_IT_GL6
#define I2C_DMA_FLAG_RX_TC      DMA1_IT_TC7
#define I2C_DMA_FLAG_RX_GL      DMA1_IT_GL7
#define I2C_TIMEOUT 10000
#define DMA_I2C_TIMEOUT I2C_TIMEOUT

#define DIRECTION_TX 1u
#define DIRECTION_RX 0u

#define EE_ADDRESS 0xA2  // 0x51 in hexa << 1 position
#define A2B_MEM_ADD 0xA0 //0x50 in hexa <<1 position

typedef enum
{
	__8_BITS_ADD = 0,
	_16_BITS_ADD = 1
} address_type;

#define RTC_ADDRESS 0xD0 //0x68 << 1

extern void i2c_write_operation(uint8_t Dev_Address, uint8_t* pBuffer, uint16_t NumByteToWrite);
extern void i2c_read_from_address(uint8_t Dev_Address, uint8_t* pBuffer, uint16_t ReadAddr, address_type add_type , uint16_t NumByteToRead);

#endif /* I2C_OPERATIONS_H_ */
