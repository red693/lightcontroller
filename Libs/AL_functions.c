/*
 * AL_functions.c
 *
 *  Created on: Dec 23, 2017
 *      Author: red
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "local_defines.h"
#include "i2c_operations.h"
#include "AL_functions.h"
#include "DS3231.h"
#include "stm32f10x.h"
#include "SoftwareTimer.h"


//local defines

//private functions
static void init_bus_Clocks(void);
static void init_GPIOs(void);
static void init_UART_interface(void);
static void init_TIMER2(void);
static void init_TIMER3(void);
static void init_IRQs(void);
static void init_ADC(void);
static void init_I2C_Interface(void);
//static void EXTI_Configuration(void);
static void led_on(void);
static void led_off(void);
static char convert(char _c);
static uint8_t hex_to_char(char *_to_convert );

static date_and_time convert_to_date_and_time(char * const _string);
static date_and_time convert_to_time_of_day(char * const _string);
static void report_time(date_and_time * time);
static void report_alarm(uint8_t alarm_number,date_and_time * time);
void report_i2c_read(uint16_t n_bytes);

//external variables

extern char rx_buffer[50];
extern uint8_t rx_wr_index;

//local variables
uint16_t CCR1_Val = 0; //30
uint16_t CCR2_Val = 0; //60
uint16_t CCR3_Val = 0; //15
uint16_t CCR4_Val = 0;  //7.5

uint16_t ADCConvertedValue = 0;

char message[50];

char string_parsing[50];
uint8_t received_command = FALSE;

extern uint8_t i2c_Rx_buffer[20];

void init_uC(void)
{
	init_bus_Clocks();
	init_GPIOs();
	init_UART_interface();
//	init_UART2_interface();
	init_TIMER2();
	init_TIMER3();
///	EXTI_Configuration();
	init_IRQs();
	init_ADC();
	init_I2C_Interface();
}

static void init_bus_Clocks(void)
{
	//enable clock for the usart
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_AFIO, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2,ENABLE);

	//Enable clock for TIMER2
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);

	//Enable clock for TIMER3
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);

	// GPIO clocks
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO | RCC_APB2Periph_GPIOB, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);

//	//spi2 clock
//	RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2 | RCC_APB2Periph_AFIO, ENABLE);
//	RCC_APB1PeriphClockCmd(SPI2, ENABLE);

	//adc clock
	RCC_ADCCLKConfig(RCC_PCLK2_Div4);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);

	//I2C clock
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, ENABLE);
}

static void init_GPIOs(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	/*Configure Led output*/
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(GPIOC, &GPIO_InitStructure);

	/*Configure grow Led output*/
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	/*Configure show Led output*/
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	/*Configure FAN output*/
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	//DEBUG PORT
	/* Configure USART1 Rx as alternate function floating */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	/* Configure USART1 Tx as alternate function push-pull */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	/*Configure I2C pin SCL*/
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_OD;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	/*Configure I2C pin SDA*/
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	// TIMER3 PWM pins
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	//ADC
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	//RTC Alarm input
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	//buttons
	//Grow LEDS button
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	//Show LEDS button
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	//Strip LEDS button
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	//FAN button
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	//Window Open sensor
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_14;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_Init(GPIOC, &GPIO_InitStructure);

}

static void init_UART_interface(void)
{
	USART_InitTypeDef Usart_conf;

	//Usart_conf.USART_BaudRate = 2000000;
	Usart_conf.USART_BaudRate = 115200;
	Usart_conf.USART_WordLength = USART_WordLength_8b;
	Usart_conf.USART_StopBits = USART_StopBits_1;
	Usart_conf.USART_Parity = USART_Parity_No;
	Usart_conf.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	Usart_conf.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

	USART_Init(USART1, &Usart_conf);
	USART_Cmd(USART1, ENABLE);

}

static void init_TIMER2(void)
{
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;

	/* TIM2 configuration */
	TIM_TimeBaseStructure.TIM_Period = 1000u;//1mSec
	TIM_TimeBaseStructure.TIM_Prescaler =63;//((SystemCoreClock/1200) - 1);//0
	TIM_TimeBaseStructure.TIM_ClockDivision = 0x0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseStructure.TIM_RepetitionCounter = 0x0000;
	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);

	TIM_Cmd(TIM2, ENABLE);
	TIM_PrescalerConfig(TIM2, 71, TIM_PSCReloadMode_Immediate);

	 /* Clear TIM2 update pending flag */
	TIM_ClearFlag(TIM2, TIM_FLAG_Update);
}

static void init_TIMER3(void)
{
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_OCInitTypeDef TIM_OCInitStructure;
	uint16_t PrescalerValue = 0;
	/* Compute the prescaler value */
	PrescalerValue = (uint16_t) 63;
	/* Time base configuration */
	TIM_TimeBaseStructure.TIM_Period = 1000;
	TIM_TimeBaseStructure.TIM_Prescaler = PrescalerValue;
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;

	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);

	TIM_Cmd(TIM3, ENABLE);

	/* PWM mode 2 = Clear on compare match */
    /* PWM mode 1 = Set on compare match */

	/* PWM1 Mode configuration: Channel1 */
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_Pulse = CCR1_Val;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;

	TIM_OC1Init(TIM3, &TIM_OCInitStructure);

	TIM_OC1PreloadConfig(TIM3, TIM_OCPreload_Enable);

	/* PWM1 Mode configuration: Channel2 */
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_Pulse = CCR2_Val;

	TIM_OC2Init(TIM3, &TIM_OCInitStructure);

	TIM_OC2PreloadConfig(TIM3, TIM_OCPreload_Enable);

	/* PWM1 Mode configuration: Channel3 */
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_Pulse = CCR3_Val;

	TIM_OC3Init(TIM3, &TIM_OCInitStructure);

	TIM_OC3PreloadConfig(TIM3, TIM_OCPreload_Enable);

	/* PWM1 Mode configuration: Channel4 */
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_Pulse = CCR4_Val;

	TIM_OC4Init(TIM3, &TIM_OCInitStructure);

	TIM_OC4PreloadConfig(TIM3, TIM_OCPreload_Enable);

	TIM_ARRPreloadConfig(TIM3, ENABLE);

	TIM_CtrlPWMOutputs(TIM3,ENABLE);

	TIM_Cmd(TIM3, ENABLE);

//	/* Clear TIM3 update pending flag */
//	TIM_ClearFlag(TIM3, TIM_FLAG_Update);

}

static void init_IRQs(void)
{
	//Interrupts configuration
	NVIC_InitTypeDef NVIC_InitStructure;

	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);

	// USART Interrupt
	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
	USART_ITConfig(USART1, USART_IT_TXE, DISABLE);

	// USART2 Interrupt
	NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
	USART_ITConfig(USART2, USART_IT_TXE, DISABLE);

	//TIMER2 Interrupt
	NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	//Enable TIM2 Update interrupt
	TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);


	 /* Configure and enable ADC interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = ADC1_2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);


	//Exterior Interrupt
//	NVIC_InitStructure.NVIC_IRQChannel = EXTI15_10_IRQn;
//	//set priority to lowest
//	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0F;
//	//set subpriority to lowest
//	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0F;
//	//enable IRQ channel
//	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
//	//update NVIC registers
//	NVIC_Init(&NVIC_InitStructure);

}

static void init_ADC(void)
{
	ADC_InitTypeDef ADC_InitStructure;
	/* ADC1 configuration ------------------------------------------------------*/
	ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;
	ADC_InitStructure.ADC_ScanConvMode = DISABLE;
	ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStructure.ADC_NbrOfChannel = 1;
	ADC_Init(ADC1, &ADC_InitStructure);
	/* ADC1 regular channels configuration */
	ADC_RegularChannelConfig(ADC1, ADC_Channel_0, 1, ADC_SampleTime_28Cycles5);
	/* Enable ADC1 EOC interrupt */
	ADC_ITConfig(ADC1, ADC_IT_EOC, ENABLE);

	/* Enable ADC1 */
	ADC_Cmd(ADC1, ENABLE);

	/* Enable ADC1 reset calibaration register */
	ADC_ResetCalibration(ADC1);
	/* Check the end of ADC1 reset calibration register */
	while(ADC_GetResetCalibrationStatus(ADC1));

	/* Start ADC1 calibaration */
	ADC_StartCalibration(ADC1);
	/* Check the end of ADC1 calibration */
	while(ADC_GetCalibrationStatus(ADC1));

	/* Start ADC1 Software Conversion */
	ADC_SoftwareStartConvCmd(ADC1, ENABLE);

}

void init_I2C_Interface(void)
{
	I2C_InitTypeDef I2C_InitStructure;

	/*!< I2C configuration */
	I2C_InitStructure.I2C_Mode = I2C_Mode_I2C;
	I2C_InitStructure.I2C_DutyCycle = I2C_DutyCycle_2;
	I2C_InitStructure.I2C_Ack = I2C_Ack_Enable;
	I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
	I2C_InitStructure.I2C_ClockSpeed = I2C_SPEED;

	/* I2C Peripheral Enable */
	I2C_Cmd(I2C1, ENABLE);
	/* Apply I2C configuration after enabling it */
	I2C_Init(I2C1, &I2C_InitStructure);

}

//static void EXTI_Configuration(void)
//{
//
//	EXTI_InitTypeDef EXTI_InitStructure;
////	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
//
//	GPIO_EXTILineConfig(GPIO_PortSourceGPIOA, GPIO_PinSource11);
//	EXTI_ClearITPendingBit(EXTI_Line11);
//
//	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
//	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
//	EXTI_InitStructure.EXTI_Line = EXTI_Line11;
//	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
//	EXTI_Init(&EXTI_InitStructure);
//
//}

void set_pwm_tim3(uint16_t ccr_value, uint8_t _channel)
{
	switch(_channel)
	{
	case 1: TIM3->CCR1 = ccr_value; break;
	case 2: TIM3->CCR2 = ccr_value; break;
	case 3: TIM3->CCR3 = ccr_value; break;
	case 4: TIM3->CCR4 = ccr_value; break;
	}
}

void led_on(void)
{
	GPIO_ResetBits(GPIOC,GPIO_Pin_13);
}

void led_off(void)
{
	GPIO_SetBits(GPIOC,GPIO_Pin_13);
}

void toggle_led(void)
{
	static BitAction bit_value = Bit_RESET;
	if (bit_value == Bit_RESET) bit_value = Bit_SET; else bit_value = Bit_RESET;
	GPIO_WriteBit(GPIOC,GPIO_Pin_13,bit_value);
}

void uart_puts(const char *s)
/* alternate print string */
{
    register char c;

    while ( (c = *s++) ) {
    	while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET)
    	    	{}
    	USART_SendData(USART1, c);
    	//	/* Loop until the end of transmission */
    	while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET)
    	{}
    }

}

char convert(char _c)
{
	char hex[] = "aAbBcCdDeEfF";
	uint8_t i;
	char result = 0;

	for(i = 0; result == 0 && hex[i] != '\0'; i++)
	{
		if(hex[i] == _c)
		{
			result = 10 + (i / 2);
		}
	}

	return result;
}

uint8_t hex_to_char(char *_to_convert )
{
	uint8_t result,i,j;
	result = 0;

	for (i = 0; i<2;i++)
	{
		result = result *16;
		if(_to_convert[i] >= '0' && _to_convert[i] <='9')
		{
			result = result + (_to_convert[i] - '0');
		}
		else
		{
			j = convert(_to_convert[i]);
			result = result + j;
		}
	}

	return result;
}

uint8_t string_to_bcd(char *_to_convert )
{
	uint8_t result;
	result = 0;

	if(_to_convert[0] >= '0' && _to_convert[0] <='9')
	{
		result  += (_to_convert[0] - '0')*16u;
	}
	if(_to_convert[1] >= '0' && _to_convert[1] <='9')
	{
		result += (_to_convert[1] - '0');
	}


	return result;
}

void translate_hex(uint8_t* destination, char* source, uint16_t length)
{
	uint16_t index = 0;


	for(index = 0; index<length;index++)
	{
		*(destination+index) =  hex_to_char(source+2*index);
	}
}

void simple_delay(uint32_t us)
{
	/* simple delay loop */
	while (us--)
	{
		asm volatile ("nop");
	}
}

void interpret_command(void)
{
	switch (string_parsing[0])
	{
	case 'c':
	{
		date_and_time time = read_date_and_time();
		report_time(&time);
		break;
	}
	case 's':
	{
		//s170113003500  sYYMMDDHHMMSS
		date_and_time time = convert_to_date_and_time(string_parsing+1);
		update_date_and_time(time);
		break;
	}
	case 'A':
	{
		//AXHHMMSS X-alarm number 1/2
		uint8_t alarm = string_parsing[1] - '0';
		date_and_time time = convert_to_time_of_day(string_parsing + 2);
		update_alarm(time,alarm);
		on_off_alarm(alarm,ON);
		break;
	}
	case 'R':
	{
//		uint8_t a_type = __8_BITS_ADD;
//		r_addr = hex_to_char(rx_buffer + 6);
//		//		sprintf(sprintf_result, "- %02X\n\r", r_addr);
//		//		uart_puts(sprintf_result);
//		i2c_read_from_address(dev_addr, i2c_Rx_buffer, r_addr, a_type, nr_bytes);
//		report_i2c_read(nr_bytes);
		break;
	}

	case 'r':
	{
		uint8_t alarm = string_parsing[1] - '0';
		date_and_time alarm_time = read_alarm(alarm);
		report_alarm(alarm, &alarm_time);
		break;
	}

	case 't':
	{
		i2c_read_from_address(RTC_ADDRESS, i2c_Rx_buffer, A1_SECONDS,__8_BITS_ADD, 4);
		report_i2c_read(4);
		break;
	}

	case 'T':
	{
		i2c_read_from_address(RTC_ADDRESS, i2c_Rx_buffer, SECONDS,__8_BITS_ADD, 19);
		report_i2c_read(19);
		break;
	}

	case 'k':
	{
		clear_alarms();
		sprintf(message,"Alarm cleared!\n\r");
		uart_puts(message);
		break;
	}

	default:
	{
		sprintf(message,"Command not recognized!\n\r");
		uart_puts(message);
		break;
	}
	}
}

void finished_command(void)
{
	memcpy(string_parsing, rx_buffer, rx_wr_index + 1);
	received_command = TRUE;
}

date_and_time convert_to_date_and_time(char * const _string)
{
	date_and_time result;
	result.year = string_to_bcd(_string);
	result.month = string_to_bcd(_string+2);
	result.month_day = string_to_bcd(_string+4);
	result.hours = string_to_bcd(_string+6);
	result.minutes = string_to_bcd(_string+8);
	result.seconds = string_to_bcd(_string+10);
	return result;
}

date_and_time convert_to_time_of_day(char * const _string)
{
	date_and_time result;
	result.hours = string_to_bcd(_string);
	result.minutes = string_to_bcd(_string+2);
	result.seconds = string_to_bcd(_string+4);
	return result;
}

static void report_time(date_and_time * time)
{

	sprintf(message, "\n\r Time is: %02d:%02d:%02d\n\r ",(*time).hours,(*time).minutes,(*time).seconds);
	uart_puts(message);
	sprintf(message, "Date is: %02d.%02d.%04d\n\r ",(*time).month_day,(*time).month,(*time).year+2000u);
	uart_puts(message);

}

static void report_alarm(uint8_t alarm_number, date_and_time * time)
{

	sprintf(message, "\n\r Alarm %1d Time is: %02d:%02d:%02d\n\r ",alarm_number, (*time).hours,(*time).minutes,(*time).seconds);
	uart_puts(message);
//	sprintf(message, "Alarm Date is: %02d.%02d.%04d\n\r ",(*time).month_day,(*time).month,(*time).year+2000u);
//	uart_puts(message);
}

void report_i2c_read(uint16_t n_bytes)
{
	uint16_t index = 0;

	sprintf(message, "%c %d ",rx_buffer[0], n_bytes);
	uart_puts(message);

	for (index = 0; index < n_bytes; index++)
	{
		sprintf(message, "%02X ",i2c_Rx_buffer[index]);
		uart_puts(message);
	}

	sprintf(message, "\n\r");
	uart_puts(message);

}
