/*
 * AL_functions.h
 *
 *  Created on: Dec 23, 2017
 *      Author: red
 */

#ifndef AL_FUNCTIONS_H_
#define AL_FUNCTIONS_H_


extern void init_uC(void);
extern void toggle_led(void);
extern void set_pwm_tim3(uint16_t ccr_value, uint8_t _channel);
extern void finished_command(void);
extern void interpret_command(void);
extern void uart_puts(const char *s);
extern void simple_delay(uint32_t us);




#endif /* LIBS_AL_FUNCTIONS_H_ */
