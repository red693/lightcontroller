/*
 * read_but.c
 *
 *  Created on: Sep 28, 2017
 *      Author: red
 */

#include <stdio.h>
#include "read_but.h"
#include "local_defines.h"
#include "SoftwareTimer.h"
#include "stm32f10x.h"



static void read_butt_values(void);


//changed state button and not treated => missed
//no change and not treated => not treated


btn_state my_buttons[NR_BUTTONS] = {[0 ... NR_BUTTONS-1] =TREATED};

uint8_t read_value[NR_BUTTONS];

void read_buttons(void)
{
	static uint8_t previous_state[NR_BUTTONS] = {[0 ... NR_BUTTONS-1] = TRUE};
	static uint16_t debouncer_timer[NR_BUTTONS];
	static uint16_t pressed_timer[NR_BUTTONS];

	read_butt_values();


	for (uint8_t i = 0u; i < NR_BUTTONS; i++)
	{

		if (previous_state[i] != read_value[i])
		{
			swTimer_set(debouncer_timer[i], DEBOUNCE_TIME);
			my_buttons[i] = DEBOUNCING;
		}
		else if (swTimer_isElapsed(debouncer_timer[i]) && read_value[i] == FALSE
				&& my_buttons[i] == DEBOUNCING)
		{
			// Pressed
			my_buttons[i] = PRESSED;
			pressed_timer[i] = 0;
		}
		else if (swTimer_isElapsed(debouncer_timer[i]) && read_value[i] == TRUE
				&& my_buttons[i] == DEBOUNCING)
		{
			//Released
			my_buttons[i] = RELEASED;
		}
		if (my_buttons[i] == PRESSED)
		{
			pressed_timer[i]++;
		}
		if (my_buttons[i] == RELEASED)
		{
			if (pressed_timer[i] < SHORT_PRESS_T)
			{
				my_buttons[i] = SHORT_PRESS;
			}
			else if (pressed_timer[i] > SHORT_PRESS_T
					&& pressed_timer[i] <= LONG_PRESS_T)
			{
				my_buttons[i] = LONG_PRESS;
			}
			else
			{
				my_buttons[i] = MISSED;
			}
		}
		swTimer_tick(debouncer_timer[i]);
		previous_state[i] = read_value[i];
	}
}

static void read_butt_values(void)
{
	read_value[GROW_BUTTON]  = GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_1);
	read_value[SHOW_BUTTON]  = GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_2);
	read_value[STRIP_BUTTON] = GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_3);
	read_value[FAN_BUTTON]   = GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_4);
}
