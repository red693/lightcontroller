/*
 * DS3231.c
 *
 *  Created on: Jan 11, 2018
 *      Author: ARosu
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "local_defines.h"
#include "i2c_operations.h"
#include "stm32f10x.h"
#include "SoftwareTimer.h"
#include "DS3231.h"

static uint8_t bcd_decode(uint8_t to_decode);

void update_date_and_time(date_and_time dat_to_write)
{
	uint8_t buffer[8];
	buffer[0] = SECONDS;
	buffer[1] = dat_to_write.seconds;
	buffer[2] = dat_to_write.minutes;
	buffer[3] = dat_to_write.hours;
	buffer[4] = dat_to_write.week_day;
	buffer[5] = dat_to_write.month_day;
	buffer[6] = dat_to_write.month;
	buffer[7] = dat_to_write.year;
	i2c_write_operation(RTC_ADDRESS, buffer,8u);
}

void update_time_of_day(date_and_time tod_to_write)
{
	uint8_t buffer[4];
	buffer[0] = SECONDS;
	buffer[1] = tod_to_write.seconds;
	buffer[2] = tod_to_write.minutes;
	buffer[3] = tod_to_write.hours;
	i2c_write_operation(RTC_ADDRESS, buffer, 4u);

}

void update_alarm(date_and_time alarm_time, uint8_t alarm_to_write)
{
	uint8_t buffer[4];
	buffer[1] = alarm_time.seconds;
	buffer[2] = alarm_time.minutes;
	buffer[3] = alarm_time.hours;
	if (alarm_to_write == 1u)
	{
		buffer[0] = A1_SECONDS;
	}
	else if (alarm_to_write == 2u)
	{
		buffer[0] = A2_MINUTES;
	}
	i2c_write_operation(RTC_ADDRESS, buffer, 4u);
}

date_and_time read_date_and_time(void)
{
	date_and_time result;
	uint8_t buffer[7];
	i2c_read_from_address(RTC_ADDRESS,buffer,SECONDS,__8_BITS_ADD,7u);
	result.seconds 		= bcd_decode(buffer[0]);
	result.minutes		= bcd_decode(buffer[1]);
	result.hours		= bcd_decode(buffer[2]);
	result.week_day		= bcd_decode(buffer[3]);
	result.month_day	= bcd_decode(buffer[4]);
	result.month		= bcd_decode(buffer[5]);
	result.year			= bcd_decode(buffer[6]);
	return result;
}

date_and_time read_alarm(uint8_t alarm_number)
{
	date_and_time result;
	uint8_t buffer[4], alarm_address = A1_SECONDS, length = 4u, offset = 0u;
	if (alarm_number == 2u)
	{
		alarm_address = A2_MINUTES;
		length = 3u;
		offset = 1u;
	}

	i2c_read_from_address(RTC_ADDRESS, buffer, alarm_address, __8_BITS_ADD, length);

	if(!offset) result.seconds = bcd_decode(buffer[0]& 0x7F); else result.seconds = 0u;
	result.minutes = bcd_decode(buffer[1-offset]& 0x7F);
	result.hours   = bcd_decode(buffer[2-offset]& 0x7F);

	return result;
}

void on_off_alarm(uint8_t alarm_number, uint8_t alarm_state)
{
	uint8_t temp,alarm_address,ctrl_reg;
	ctrl_reg = read_reg(DS3231_CTRL);

	if(alarm_number == 1u)
	{
		temp = read_reg(A1_DAY_DATE);
		alarm_address = A1_DAY_DATE;
	} else if (alarm_number == 2u)
	{
		temp = read_reg(A2_DAY_DATE);
		alarm_address = A2_DAY_DATE;
	}

	if (alarm_state == ON)
	{
		SETBIT(temp,ALARM_ON_BIT);
		SETBIT(ctrl_reg,alarm_number-1u);
	}
	else
	{
		CLEARBIT(temp,ALARM_ON_BIT);
		CLEARBIT(ctrl_reg,alarm_number-1u);
	}
	write_reg(alarm_address,temp);
	write_reg(DS3231_CTRL,ctrl_reg);


}

void clear_alarms(void)
{
	uint8_t status_reg;
	status_reg = read_reg(DS3231_STATUS);
	status_reg &=0xFC; // clear last two bits the flags of the alarms
	write_reg(DS3231_STATUS,status_reg);
}

uint8_t read_temperature(void)
{
	uint8_t result = 0u;
	i2c_read_from_address(RTC_ADDRESS,&result,TEMPERATURE_MSB,__8_BITS_ADD,1u);
	return result;
}

void write_reg(uint8_t address, uint8_t content)
{
	uint8_t buffer[2];
	buffer[0] = address;
	buffer[1] = content;
	i2c_write_operation(RTC_ADDRESS, buffer, 2u);
}

uint8_t read_reg(uint8_t address)
{
	uint8_t result = 0u;
	i2c_read_from_address(RTC_ADDRESS,&result,address,__8_BITS_ADD,1u);
	return result;
}

static uint8_t bcd_decode(uint8_t to_decode)
{
	uint8_t result = 0u;
	result = (to_decode >>4)*10u;
	result+= (to_decode & 0x0F);
	return result;
}

