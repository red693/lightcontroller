/*
 * DS3231.h
 *
 *  Created on: Jan 11, 2018
 *      Author: ARosu
 */

#ifndef DS3231_H_
#define DS3231_H_

#define SECONDS 		0x00
#define MINUTES 		0x01
#define HOURS 			0x02
#define DAY				0x03
#define DATE			0x04
#define MONTH			0x05
#define YEAR			0x06
#define A1_SECONDS 		0x07
#define A1_MINUTES 		0x08
#define A1_HOURS 		0x09
#define A1_DAY_DATE		0x0A
#define A2_MINUTES 		0x0B
#define A2_HOURS 		0x0C
#define A2_DAY_DATE		0x0D
#define DS3231_CTRL		0x0E
#define DS3231_STATUS	0x0F
#define AGING_OFFSET	0x10
#define TEMPERATURE_MSB	0x11
#define TEMPERATURE_LSB	0x12

#define ALARM_ON_BIT 7u


typedef struct
{
	uint8_t seconds;
	uint8_t minutes;
	uint8_t hours;
	uint8_t week_day;
	uint8_t month_day;
	uint8_t month;
	uint8_t year;
} date_and_time;


extern void update_date_and_time(date_and_time dat_to_write);
extern void update_time_of_day(date_and_time tod_to_write);
extern void update_alarm(date_and_time alarm_time, uint8_t alarm_to_write);
extern date_and_time read_date_and_time(void);
extern date_and_time read_alarm(uint8_t alarm_number);
extern void on_off_alarm(uint8_t alarm_number, uint8_t alarm_state);
extern uint8_t read_temperature(void);
extern void write_reg(uint8_t address, uint8_t content);
extern uint8_t read_reg(uint8_t address);
extern void clear_alarms(void);

#endif /* DS3231_H_ */
