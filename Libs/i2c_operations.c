/*
 * i2c_operations.c
 *
 *  Created on: Jan 13, 2018
 *      Author: red
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "local_defines.h"
#include "i2c_operations.h"
#include "stm32f10x.h"
#include "SoftwareTimer.h"


static void i2c_reinit(void);
//static void report_i2c_read(uint16_t n_bytes);
static void i2c_read_operation(uint8_t Dev_Address, uint8_t* pBuffer, uint16_t NumByteToRead, uint8_t I2C_on);
static void I2C_Timeout_UserCallback(char* _message);
static void _delay(uint32_t us);
//external variables

extern char message[50];

//local variables
uint32_t i2c_timer = 0;

uint8_t I2C_error = FALSE;
uint8_t I2C_op_counter = 0;

uint8_t i2c_Rx_buffer[20];


void i2c_read_from_address(uint8_t Dev_Address, uint8_t* pBuffer, uint16_t ReadAddr, address_type add_type , uint16_t NumByteToRead)
{
	I2C_error = FALSE;
	/*!< While the bus is busy */
	swTimer_set(i2c_timer, I2C_TIMEOUT);
	while (I2C_GetFlagStatus(I2C1, I2C_FLAG_BUSY) && I2C_error == FALSE)
	{
		swTimer_tick(i2c_timer);
		if (swTimer_isElapsed(i2c_timer))
			I2C_Timeout_UserCallback("start read but I2C unit busy!");
	}


	if (I2C_error == FALSE)
	{
		/*!< Send START condition */
		I2C_GenerateSTART(I2C1, ENABLE);
		/*!< Test on EV5 and clear it (cleared by reading SR1 then writing to DR) */
		swTimer_set(i2c_timer, I2C_TIMEOUT);
		while (!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_MODE_SELECT) && I2C_error == FALSE)
		{
			swTimer_tick(i2c_timer);
			if (swTimer_isElapsed(i2c_timer))
				I2C_Timeout_UserCallback("read sending start condition!");
		}
	}

	if (I2C_error == FALSE)
	{
		/*!< Send slave address for write */
		I2C_Send7bitAddress(I2C1, Dev_Address, I2C_Direction_Transmitter);

		/*!< Test on EV6 and clear it */
		swTimer_set(i2c_timer, I2C_TIMEOUT);
		while (!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED) && I2C_error == FALSE)
		{
			swTimer_tick(i2c_timer);
			if (swTimer_isElapsed(i2c_timer))
				I2C_Timeout_UserCallback("slave response to address!");
		}
	}

	if (add_type == _16_BITS_ADD && I2C_error == FALSE)
	{
		/*!< Send the slave internal address to read from: MSB of the address first */
		I2C_SendData(I2C1, (uint8_t) ((ReadAddr & 0xFF00) >> 8));

		/*!< Test on EV8 and clear it */
		swTimer_set(i2c_timer, I2C_TIMEOUT);
		while (!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_TRANSMITTED) && I2C_error == FALSE)
		{
			swTimer_tick(i2c_timer);
			if (swTimer_isElapsed(i2c_timer))
				I2C_Timeout_UserCallback("read address most significant byte!");
		}
	}

	if (I2C_error == FALSE)
	{/*!< Send the slave internal address to read from: LSB of the address */
		I2C_SendData(I2C1, (uint8_t) (ReadAddr & 0x00FF));
		/*!< Test on EV8 and clear it */
		swTimer_set(i2c_timer, I2C_TIMEOUT);
		while (I2C_GetFlagStatus(I2C1, I2C_FLAG_BTF) == RESET && I2C_error == FALSE)
		{
			swTimer_tick(i2c_timer);
			if (swTimer_isElapsed(i2c_timer))
				I2C_Timeout_UserCallback("read address least significant byte!");
		}
	}

	if (I2C_error == FALSE)
		i2c_read_operation(Dev_Address, pBuffer, NumByteToRead, TRUE);
}

void i2c_read_operation(uint8_t Dev_Address, uint8_t* pBuffer, uint16_t NumByteToRead, uint8_t I2C_on)
{
	uint16_t counter = 0;
	I2C_error = FALSE;
	/*!< While the bus is busy */
	if (I2C_on == FALSE)
	{
		swTimer_set(i2c_timer, I2C_TIMEOUT);
		while (I2C_GetFlagStatus(I2C1, I2C_FLAG_BUSY) && I2C_error == FALSE)
		{
			swTimer_tick(i2c_timer);
			if (swTimer_isElapsed(i2c_timer))
				I2C_Timeout_UserCallback("start read but I2C unit busy!");
		}
	}

	if(I2C_error == FALSE)
	{
	/*!< Send START condition a second time */
		I2C_GenerateSTART(I2C1, ENABLE);

		/*!< Test on EV5 and clear it (cleared by reading SR1 then writing to DR) */
		swTimer_set(i2c_timer, I2C_TIMEOUT);
		while (!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_MODE_SELECT) && I2C_error == FALSE)
		{
			swTimer_tick(i2c_timer);
			if (swTimer_isElapsed(i2c_timer))
				I2C_Timeout_UserCallback("read device start condition!");
		}
	}

	if (I2C_error == FALSE)
	{
		/*!< Send device address for read */
		I2C_Send7bitAddress(I2C1, Dev_Address, I2C_Direction_Receiver);
		/*!< Test on EV6 and clear it */
		swTimer_set(i2c_timer, I2C_TIMEOUT);
		while (!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED) && I2C_error == FALSE)
		{
			swTimer_tick(i2c_timer);
			if (swTimer_isElapsed(i2c_timer))
				I2C_Timeout_UserCallback("read slave no response to address!");
		}
	}

	while (counter <= NumByteToRead - 2 && I2C_error == FALSE)
	{
		swTimer_set(i2c_timer, I2C_TIMEOUT);
		while (!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_RECEIVED) && I2C_error == FALSE)
		{
			swTimer_tick(i2c_timer);
			if (swTimer_isElapsed(i2c_timer))
				I2C_Timeout_UserCallback("read byte loop. Byte not received!");
		}

		/*!< Read the byte received from the EEPROM */
		pBuffer[counter] = I2C_ReceiveData(I2C1);

		/*!< Decrement the read bytes counter */
		counter++;
		/* Clear ADDR register by reading SR1 then SR2 register (SR1 has already been read) */
		(void) I2C1->SR2;
	}

	if (I2C_error == FALSE)
	{	//last byte
		/*!< Disable Acknowledgment */
		I2C_AcknowledgeConfig(I2C1, DISABLE);

		/* Clear ADDR register by reading SR1 then SR2 register (SR1 has already been read) */
		(void) I2C1->SR2;

		/*!< Send STOP Condition */
		I2C_GenerateSTOP(I2C1, ENABLE);

		/* Wait for the byte to be received */
		swTimer_set(i2c_timer, I2C_TIMEOUT);
		while (I2C_GetFlagStatus(I2C1, I2C_FLAG_RXNE) == RESET && I2C_error == FALSE)
		{
			swTimer_tick(i2c_timer);
			if (swTimer_isElapsed(i2c_timer))
				I2C_Timeout_UserCallback("read operation end - STOP not sent!");
		}

		/*!< Read the byte received from the EEPROM */
		pBuffer[NumByteToRead - 1] = I2C_ReceiveData(I2C1);
		/*!< Re-Enable Acknowledgment to be ready for another reception */
	}

	I2C_AcknowledgeConfig(I2C1, ENABLE);
	i2c_reinit();
}

void i2c_write_operation(uint8_t Dev_Address, uint8_t* pBuffer, uint16_t NumByteToWrite)
{
	uint16_t counter = 0u;
	I2C_error = FALSE;
	/*!< While the bus is busy */
	swTimer_set(i2c_timer, I2C_TIMEOUT);
	while (I2C_GetFlagStatus(I2C1, I2C_FLAG_BUSY) && I2C_error == FALSE)
	{
		swTimer_tick(i2c_timer);
		if (swTimer_isElapsed(i2c_timer))
			I2C_Timeout_UserCallback("write because I2C unit busy!");
	}

	if (I2C_error == FALSE)
	{
		/*!< Send START condition */
		I2C_GenerateSTART(I2C1, ENABLE);

		/*!< Test on EV5 and clear it */
		swTimer_set(i2c_timer, I2C_TIMEOUT);
		while (!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_MODE_SELECT) && I2C_error == FALSE)
		{
			swTimer_tick(i2c_timer);
			if (swTimer_isElapsed(i2c_timer))
				I2C_Timeout_UserCallback("write - start condition I2C!");
		}
	}

	if (I2C_error == FALSE)
	{
		/*!< Send EEPROM address for write */
		I2C_Send7bitAddress(I2C1, Dev_Address, I2C_Direction_Transmitter);

		/*!< Test on EV6 and clear it */
		swTimer_set(i2c_timer, I2C_TIMEOUT);
		while (!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED) && I2C_error == FALSE)
		{
			swTimer_tick(i2c_timer);
			if (swTimer_isElapsed(i2c_timer))
			{
				I2C_Timeout_UserCallback("write - device not answer to own address!");
			}
		}
	}
	while (counter < NumByteToWrite && I2C_error == FALSE)
	{
		I2C_SendData(I2C1, pBuffer[counter]);
		/*wait for byte send to complete*/
		swTimer_set(i2c_timer, I2C_TIMEOUT);
		while (!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_TRANSMITTED) && I2C_error == FALSE)
		{
			swTimer_tick(i2c_timer);
			if (swTimer_isElapsed(i2c_timer))
				I2C_Timeout_UserCallback("writing next byte!");
		}
		counter++;
	}
	(void) I2C1->SR1;
	(void) I2C1->SR2;

	if (I2C_error == FALSE)
	{
		/*!< Send STOP condition */
		I2C_GenerateSTOP(I2C1, ENABLE);
		swTimer_set(i2c_timer, I2C_TIMEOUT);
		while (I2C_GetFlagStatus(I2C1, I2C_FLAG_STOPF) && I2C_error == FALSE)
		{
			swTimer_tick(i2c_timer);
			if (swTimer_isElapsed(i2c_timer))
				I2C_Timeout_UserCallback("write end- Stop not possible!");
		}
	}
	I2C_op_counter++;
}


void check_i2c_counter(void)
{
	if (I2C_op_counter >= I2C_OP_LIMIT)
	{
		i2c_reinit();
	}

}

void i2c_reinit(void)
{
	I2C_InitTypeDef I2C_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;

	I2C_DeInit(I2C1);
	/* I2C Peripheral Disable */
	I2C_Cmd(I2C1, DISABLE);

	/*Re-Configure I2C pin SCL*/
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	/*Re-Configure I2C pin SCL*/
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	_delay(10);

	/*Re-Configure I2C pin SCL*/
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_OD;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	/*Re-Configure I2C pin SCL*/
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	/*toggle bit I2C reset*/
	I2C_SoftwareResetCmd(I2C1, ENABLE);
	_delay(10);
	I2C_SoftwareResetCmd(I2C1,DISABLE);

	/*!< I2C configuration */
	I2C_InitStructure.I2C_Mode = I2C_Mode_I2C;
	I2C_InitStructure.I2C_DutyCycle = I2C_DutyCycle_2;
	I2C_InitStructure.I2C_Ack = I2C_Ack_Enable;
	I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
	I2C_InitStructure.I2C_ClockSpeed = I2C_SPEED;

	/* Apply I2C configuration after enabling it */
	I2C_Init(I2C1, &I2C_InitStructure);

	I2C_Cmd(I2C1, ENABLE);
	I2C_op_counter = 0;
//	sprintf(sprintf_result,"I2C reset \n\r");
//	uart_puts(sprintf_result);
}


//void report_i2c_read(uint16_t n_bytes)
//{
//	uint16_t index = 0;
//
//	sprintf(message, "%c %d ",rx_buffer[0], n_bytes);
//	uart_puts(message);
//
//	for (index = 0; index < n_bytes; index++)
//	{
//		sprintf(message, "%02X ",i2c_Rx_buffer[index]);
//		uart_puts(message);
//	}
//
//	sprintf(message, "\n\r");
//	uart_puts(message);
//
//}

void I2C_Timeout_UserCallback(char* _message)
{
	sprintf(message, "Communication Error in %s\n\r", _message);
	uart_puts(message);
	I2C_error = TRUE;
}

void _delay(uint32_t us)
{
	/* simple delay loop */
	while (us--)
	{
		asm volatile ("nop");
	}
}
