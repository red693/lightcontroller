/*
 * SoftwareTimer.h
 *
 *  Created on: Aug 1, 2017
 *      Author: ARosu
 */

#ifndef SOFTWARETIMER_H_
#define SOFTWARETIMER_H_


/*************************************************************************************************/
/*!
\brief
Macro for ticking a software timer (timer is decreased by 1 if greater than 0)
*/
#define swTimer_tick(TIMER) if (TIMER > 0) {(TIMER)--;}

/*************************************************************************************************/
/*!
\brief
Macro for checking if a timer has elapsed (value equals 0).
*/
#define swTimer_isElapsed(TIMER) ((TIMER) == 0)

/*************************************************************************************************/
/*!
\brief
Macro for loading a timer with a value.
*/
#define swTimer_set(TIMER,VALUE) ((TIMER) = VALUE)

/*************************************************************************************************/
/*!
\brief
Macro for stopping a timer (value is set to 0).
*/
#define swTimer_stop(TIMER) ((TIMER) = 0)

/*************************************************************************************************/
/*!
\brief
Macro for computing a timeout value based on the time and the task recurrence.
*/
#define	SW_TIMER_COMPUTE_TIMEOUT(TIME,TASK_RECURRENCE) ((TIME)/(TASK_RECURRENCE))

#endif /* SOFTWARETIMER_H_ */
