/**
  ******************************************************************************
  * @file    main_controller.c
  * @author  Ac6
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "AL_functions.h"
#include "Leds.h"
#include "stm32f10x.h"
#include "SoftwareTimer.h"
#include "local_defines.h"
#include "read_but.h"
#include "main_controller.h"
#include "DS3231.h"


//local defines

#define FAN_ON_TIME 60u*4000u //minutes*(ticks/minute)
#define FAN_OFF_TIME 11u*60u*4000u // hours*minutes*(ticks/minute)

#define FULL_STRIP_ON_TIME 10u*4000u //minutes*(ticks/minute)
#define STRIP_GLOW_TIME 5u*60u*4000u // hours*minutes*(ticks/minute)
#define STRIP_GLOW_STEP_TIME 5u // 10*(1/3 glow cycle = 15ms*20steps)
//#define STRIP_OFF_TIME ((STRIP_PWM_MAX/STRIP_RAMP_FULL_ON_STEP)*STRIP_RAMP_FULL_ON_TIME)
#define STRIP_OFF_TIME 1000u

#define SHOW_ON_TIME 5u*60u*4000u // hours*minutes*(ticks/minute)
#define SHOW_ON_DEMO_TIME 60u*4000u // minutes*(ticks/minute)

#define GROW_ON_TIME 2u*60u*4000u // hours*minutes*(ticks/minute)

#define STANDBY_TIME	34u //approx 500ms

#define UNDERVOLTAGE_DAC_LIMIT 2050u //11.8V - resistor division factor of 7.24
#define LOWPOWER_DAC_LIMIT 2135u //12.3V
#define NOMINAL_DAC_LIMIT 2222u //12.8V
#define CHARGED_DAC_LIMIT 2604u //15V

// local types
typedef enum
{
	CTRL_STANDBY,
	CTRL_GROW_ON,
	CTRL_DECIDE,
	CTRL_SHOW_ON
} main_controller_state;

typedef enum
{
	LS_IDLE,
	LS_GLOW,
	LS_FULL_ON,
	LS_FULL_OFF
} led_strip_state;

typedef enum
{
	FAN_IDLE,
	FAN_ON
} fan_state;

typedef enum
{
	NS_STRIP,
	NS_WS2812
} show_state;

typedef enum
{
	UNDERVOLTAGE,
	LOWPOWER,
	NOMINAL,
	CHARGING,
	CHARGED
} vbat;

// private functions
static void _io_task(void);
static void controller_state_machine(void);
static void strip_leds_sm(void);
static void strip_leds_idle_state(void);
static void strip_leds_glow_state(void);
static void strip_leds_full_on_state(void);
static void strip_leds_full_off_state(void);
static void fan_sm(void);
static void fan_idle_state(void);
static void fan_on_state(void);
static void standby_state(void);
static void control_grow_leds_on_state(void);
static void control_decide_state(void);
static void control_show_leds_on_state(void);
static void update_leds(void);
static void treat_buttons(void);
static void check_alarm(void);

// external variables
extern btn_state my_buttons[NR_BUTTONS];
extern uint16_t ADCConvertedValue;
extern char message[50];

// local variables
uint8_t onboard_led = ACTIVE;
uint8_t strip_leds = INACTIVE;
uint8_t grow_leds = INACTIVE;
uint8_t show_leds = INACTIVE;
uint8_t fan = INACTIVE;
uint8_t strip_leds_forced = INACTIVE;
uint8_t grow_leds_forced = INACTIVE;
uint8_t show_leds_forced = INACTIVE;
uint8_t fan_forced = INACTIVE;
uint8_t adc_state = ACTIVE;
uint8_t alarm = INACTIVE;
uint16_t standby_timer = 0;
vbat battery_state = NOMINAL;
vbat previous_bat_state = NOMINAL;

main_controller_state controller_state = CTRL_STANDBY;
led_strip_state _led_strip_state = LS_IDLE;
fan_state _fan_state = FAN_IDLE;
show_state next_show = NS_STRIP;
uint32_t controller_state_timer, strip_state_timer, fan_state_timer;
uint8_t strip_off_channel = ALL_CHANNELS;

uint8_t debug = TRUE;

void main_control_task15ms(void)
{
	_io_task();
	controller_state_machine();
	strip_leds_sm();
	fan_sm();
}

static void _io_task(void)
{
	check_alarm();
	read_buttons();
	treat_buttons();
	update_leds();
}

static void controller_state_machine(void)
{
	swTimer_tick(controller_state_timer);
	switch (controller_state)
	{
	case CTRL_STANDBY:
		standby_state();
		break;
	case CTRL_GROW_ON:
		control_grow_leds_on_state();
		break;
	case CTRL_DECIDE:
		control_decide_state();
		break;
	case CTRL_SHOW_ON:
		control_show_leds_on_state();
		break;
	default:
	{
		controller_state = CTRL_STANDBY;
		break;
	}
	}
}

static void standby_state(void)
{
	//action

	//transitions
	//battery charged
	if (battery_state == CHARGED)
	{
		controller_state = CTRL_GROW_ON;
		grow_leds = ACTIVE;
		swTimer_set(controller_state_timer,GROW_ON_TIME);
		if (debug)
		{
			sprintf(message, "Full Charge Detected!\n\r");
			uart_puts(message);
			sprintf(message, "Turning GROW Leds On!\n\r");
			uart_puts(message);
		}
	}
	//Alarm detected
	if (alarm == ACTIVE)
	{
		controller_state = CTRL_DECIDE;
		if (debug)
		{
			sprintf(message, "Alarm Detected!\n\rDeciding!\n\r");
			uart_puts(message);
		}
	}
	//Show button
	if (my_buttons[SHOW_BUTTON] == PRESSED)
	{
		controller_state = CTRL_SHOW_ON;
		show_leds = ACTIVE;
		swTimer_set(controller_state_timer, SHOW_ON_DEMO_TIME);
		my_buttons[SHOW_BUTTON] = TREATED;
		if (debug)
		{
			sprintf(message, "Show Button Pressed!\n\r");
			uart_puts(message);
			sprintf(message, "Show leds on!\n\r");
			uart_puts(message);
		}
	}
}

static void control_grow_leds_on_state(void)
{
	//action

	//transitions
	//battery charged
	if (battery_state != CHARGED)
	{
		controller_state = CTRL_STANDBY;
		grow_leds = INACTIVE;
		if (debug)
		{
			sprintf(message, "Battery Grow Limit Reached!\n\r");
			uart_puts(message);
		}
	}
	if (swTimer_isElapsed(controller_state_timer))
	{
		controller_state = CTRL_STANDBY;
		grow_leds = INACTIVE;
		if (debug)
		{
			sprintf(message, "Grow Time Limit Reached!\n\rEnter StandBy!\n\r");
			uart_puts(message);
		}
	}
	if (alarm == ACTIVE)
	{
		controller_state = CTRL_STANDBY;
		grow_leds = INACTIVE;
		if (debug)
		{
			sprintf(message, " Alarm detected!\n\rEnter StandBy!\n\r");
			uart_puts(message);
		}
	}
}

static void control_decide_state(void)
{
	//action

	//transitions
	if (battery_state != LOWPOWER && battery_state != UNDERVOLTAGE && next_show == NS_STRIP)
	{
		show_leds = ACTIVE;
		next_show = NS_WS2812;
		controller_state = CTRL_STANDBY;
		if (debug)
		{
			sprintf(message, "Activating strip leds!\n\r");
			uart_puts(message);
		}
	}
	else if (battery_state != UNDERVOLTAGE && next_show == NS_WS2812)
	{
		strip_leds = ACTIVE;
		next_show = NS_STRIP;
		controller_state = CTRL_SHOW_ON;
		swTimer_set(controller_state_timer,SHOW_ON_TIME);
		if (debug)
		{
			sprintf(message, "Activating show leds!\n\r");
			uart_puts(message);
		}
	}
	else
	{
		controller_state = CTRL_STANDBY;
		if (debug)
		{
			sprintf(message, "No decision!\n\rCheck Battery!\n\r");
			uart_puts(message);
		}
	}
	clear_alarms();
	sprintf(message, "Alarm cleared!\n\r");
	uart_puts(message);
}

static void control_show_leds_on_state(void)
{
	//action

	//transitions
	// show time over
	if (swTimer_isElapsed(controller_state_timer))
	{
		controller_state = CTRL_STANDBY;
		show_leds = INACTIVE;
		if (debug)
		{
			sprintf(message, "Show Time over!\n\rStand By!\n\r");
			uart_puts(message);
		}
	}
	//button
	if (my_buttons[SHOW_BUTTON] == PRESSED)
	{
		controller_state = CTRL_STANDBY;
		show_leds = INACTIVE;
		my_buttons[SHOW_BUTTON] = TREATED;
		if (debug)
		{
			sprintf(message, "Show Button Pressed!\n\r");
			uart_puts(message);
			sprintf(message, "Show leds OFF!\n\r");
			uart_puts(message);
		}
	}
}

static void strip_leds_sm(void)
{
	swTimer_tick(strip_state_timer);
	switch (_led_strip_state)
	{
	case LS_IDLE:
		{
			strip_leds_idle_state();
			break;
		}
	case LS_GLOW:
		{
			strip_leds_glow_state();
			break;
		}
	case LS_FULL_ON:
		{
			strip_leds_full_on_state();
			break;
		}
	case LS_FULL_OFF:
		{
			strip_leds_full_off_state();
			break;
		}
	default:
		_led_strip_state = LS_IDLE;
	}
}

static void strip_leds_idle_state(void)
{
	//action

	//transitions
	//button pressed
	if (my_buttons[STRIP_BUTTON] == PRESSED)
	{
		_led_strip_state = LS_FULL_ON;
		swTimer_set(strip_state_timer,FULL_STRIP_ON_TIME);
		my_buttons[STRIP_BUTTON] = TREATED;
		if (debug)
		{
			sprintf(message, "Strip Button Pressed!\n\r");
			uart_puts(message);
			sprintf(message, "Full leds on!\n\r");
			uart_puts(message);
		}
	}
	//alarm detected and main controller starts the glowing
	if (strip_leds == ACTIVE)
	{
		_led_strip_state = LS_GLOW;
		swTimer_set(strip_state_timer, STRIP_GLOW_TIME);
		if (debug)
		{
			sprintf(message, "Starting GLOW mode!\n\r");
			uart_puts(message);
		}
	}

}

static void strip_leds_glow_state(void)
{
	//action
	glow(STRIP_GLOW_STEP_TIME);
	//transition
	//button pressed
	if (my_buttons[STRIP_BUTTON] == PRESSED)
	{
		_led_strip_state = LS_FULL_OFF;
		strip_off_channel = get_glow_color();
		swTimer_set(strip_state_timer, STRIP_OFF_TIME);
		my_buttons[STRIP_BUTTON] = TREATED;
		if (debug)
		{
			sprintf(message, "Strip Button Pressed!\n\r");
			uart_puts(message);
			sprintf(message, "Full leds off!\n\r");
			uart_puts(message);
		}
	}

	// alarm timeout
	if (swTimer_isElapsed(strip_state_timer))
	{
		_led_strip_state = LS_FULL_OFF;
		strip_off_channel = get_glow_color();
		swTimer_set(strip_state_timer, STRIP_OFF_TIME);
		if (debug)
		{
			sprintf(message, "Time to turn off Strip!\n\r");
			uart_puts(message);
		}
	}
}

static void strip_leds_full_on_state(void)
{
	//action
	strip_full_ramp_on_task_15ms();
	//transition
	//button pressed
	if (my_buttons[STRIP_BUTTON] == PRESSED)
	{
		strip_full_off();
		_led_strip_state = LS_GLOW;
		swTimer_set(strip_state_timer, STRIP_GLOW_TIME);
		my_buttons[STRIP_BUTTON] = TREATED;
		if (debug)
		{
			sprintf(message, "Strip Button Pressed!\n\r");
			uart_puts(message);
			sprintf(message, "Glowing leds!\n\r");
			uart_puts(message);
		}
	}

	//timeout for lights on
	if (swTimer_isElapsed(strip_state_timer))
	{
		strip_off_channel = ALL_CHANNELS;
		_led_strip_state = LS_FULL_OFF;
		swTimer_set(strip_state_timer, STRIP_OFF_TIME);
		if (debug)
		{
			sprintf(message, "Time to turn off Strip!\n\r");
			uart_puts(message);
		}
	}

}

static void strip_leds_full_off_state(void)
{
	//action
	strip_full_ramp_off_task_15ms(strip_off_channel);
	//transition
	if (my_buttons[STRIP_BUTTON] == PRESSED)
	{
		_led_strip_state = LS_IDLE;
		//my_buttons[STRIP_BUTTON] = TREATED;
		if (debug)
		{
			sprintf(message, "Strip Button Pressed!\n\r");
			uart_puts(message);
			sprintf(message, "Going to IDLE!\n\r");
			uart_puts(message);
		}
	}
	// move to standby after ramp down
	if (swTimer_isElapsed(strip_state_timer))
	{
		_led_strip_state = LS_IDLE;
		if (debug)
		{
			sprintf(message, "Timeout!\n\r Going to IDLE!\n\r");
			uart_puts(message);
		}
	}
}

static void fan_sm(void)
{
	swTimer_tick(fan_state_timer);
	switch (_fan_state)
	{
	case FAN_IDLE:
	{
		fan_idle_state();
		break;
	}
	case FAN_ON:
	{
		fan_on_state();
		break;
	}
	default:
	{
		fan_state_timer = FAN_IDLE;
		break;
	}
	}

}

static void fan_idle_state(void)
{
	//action
	//transition
	// on button pressed
	if (my_buttons[FAN_BUTTON] == PRESSED)
	{
		_fan_state = FAN_ON;
		my_buttons[FAN_BUTTON] = TREATED;
		swTimer_set(fan_state_timer, FAN_ON_TIME);
		FAN_ON();
		if (debug)
		{
			sprintf(message, "FAN Button Pressed!\n\r");
			uart_puts(message);
			sprintf(message, "Start FAN and go to FAN_ON!\n\r");
			uart_puts(message);
		}
	}
	// move to on after stand_by time passed
	if (swTimer_isElapsed(fan_state_timer))
	{
		FAN_ON();
		swTimer_set(fan_state_timer, FAN_ON_TIME);
		_fan_state = FAN_ON;
		if (debug)
		{
			sprintf(message, "Time to start FAN!\n\r Going to ON!\n\r");
			uart_puts(message);
		}
	}
}

static void fan_on_state(void)
{
	//action
	//transition
	// on button pressed
	if (my_buttons[FAN_BUTTON] == PRESSED)
	{
		_fan_state = FAN_IDLE;
		my_buttons[FAN_BUTTON] = TREATED;
		swTimer_set(fan_state_timer, FAN_OFF_TIME);
		FAN_OFF();
		if (debug)
		{
			sprintf(message, "FAN Button Pressed!\n\r");
			uart_puts(message);
			sprintf(message, "Stop FAN and go to FAN_IDLE!\n\r");
			uart_puts(message);
		}
	}
	// move to standby after ramp down
	if (swTimer_isElapsed(fan_state_timer))
	{
		FAN_OFF();
		swTimer_set(fan_state_timer, FAN_OFF_TIME);
		_fan_state = FAN_IDLE;
		if (debug)
		{
			sprintf(message, "Time to stop FAN!\n\r Going to IDLE!\n\r");
			uart_puts(message);
		}
	}
}

void adc_task15ms(void)
{
	if (swTimer_isElapsed(standby_timer))
	{
		if (adc_state == ACTIVE)
		{
			swTimer_set(standby_timer, STANDBY_TIME);
			//ADC_Cmd(ADC1, DISABLE);
			if (ADCConvertedValue > CHARGED_DAC_LIMIT)
			{
				battery_state = CHARGED;
				if (debug && previous_bat_state!=CHARGED)
				{
					sprintf(message, "CHARGED!\n\r");
					uart_puts(message);

				}
			}
			else if ((ADCConvertedValue > NOMINAL_DAC_LIMIT)
					&& (ADCConvertedValue < CHARGED_DAC_LIMIT))
			{
				battery_state = CHARGING;
				if (debug&& previous_bat_state!=CHARGING)
				{
					sprintf(message, "CHARGING!\n\r");
					uart_puts(message);

				}
			}
			else if ((ADCConvertedValue > LOWPOWER_DAC_LIMIT)
					&& (ADCConvertedValue < NOMINAL_DAC_LIMIT))
			{
				battery_state = NOMINAL;
				if (debug&& previous_bat_state!=NOMINAL)
				{
					sprintf(message, "NOMINAL!\n\r");
					uart_puts(message);

				}
			}
			else if ((ADCConvertedValue > UNDERVOLTAGE_DAC_LIMIT)
					&& (ADCConvertedValue < LOWPOWER_DAC_LIMIT))
			{
				battery_state = LOWPOWER;
				if (debug && previous_bat_state!=LOWPOWER)
				{
					sprintf(message, "LOWPOWER!\n\r");
					uart_puts(message);

				}
			}
			else if ((ADCConvertedValue < UNDERVOLTAGE_DAC_LIMIT))
			{
				battery_state = UNDERVOLTAGE;
				if (debug && previous_bat_state!=UNDERVOLTAGE)
				{
					sprintf(message, "UNDERVOLTAGE!\n\r");
					uart_puts(message);

				}
			}
			previous_bat_state = battery_state;
		}
		else
		{
			//ADC_Cmd(ADC1, ENABLE);
		}
	}
	else
	{
		swTimer_tick(standby_timer);
	}
}

static void check_alarm(void)
{
	static uint8_t stated = FALSE, previous = FALSE;
	if ((CHECK_ALARM()) == ACTIVE && previous == INACTIVE)
	{
		alarm = ACTIVE;
	}

	if ((CHECK_ALARM()) == INACTIVE && previous == ACTIVE)
	{
		alarm = INACTIVE;
		stated = FALSE;
	}

	if (debug && stated == FALSE && alarm == ACTIVE)
	{
		stated = TRUE;
		sprintf(message, "Alarm ON!\n\r");
		uart_puts(message);
	}

	previous = alarm;
}


static void treat_buttons(void)
{
	if (my_buttons[GROW_BUTTON] == PRESSED)
	{
		if (grow_leds_forced == ACTIVE)
			grow_leds_forced = INACTIVE;
		else if (grow_leds_forced == INACTIVE)
			grow_leds_forced = ACTIVE;
		my_buttons[GROW_BUTTON] = TREATED;
		if (debug)
		{
			sprintf(message, "Grow Button Pressed!\n\r");
			uart_puts(message);
		}
	}

//	if (my_buttons[SHOW_BUTTON] == PRESSED)
//	{
//		if (show_leds_forced == ACTIVE)
//			show_leds_forced = INACTIVE;
//		else if (show_leds_forced == INACTIVE)
//			show_leds_forced = ACTIVE;
//		my_buttons[SHOW_BUTTON] = TREATED;
//		if (debug)
//		{
//			sprintf(message, "Show Button Pressed!\n\r");
//			uart_puts(message);
//		}
//	}

//	if (my_buttons[FAN_BUTTON] == PRESSED)
//	{
//		if (fan_forced == ACTIVE)
//			fan_forced = INACTIVE;
//		else if (fan_forced == INACTIVE)
//			fan_forced = ACTIVE;
//		my_buttons[FAN_BUTTON] = TREATED;
//		if (debug)
//		{
//			sprintf(message, "FAN Button Pressed!\n\r");
//			uart_puts(message);
//		}
//	}
}

static void update_leds(void)
{
	if (grow_leds == ACTIVE || grow_leds_forced == ACTIVE)
	{
		activate_grow_leds();
	}
	else if (grow_leds == INACTIVE && grow_leds_forced == INACTIVE)
	{
		deactivate_grow_leds();
	}

	if (show_leds == ACTIVE || show_leds_forced == ACTIVE)
	{
		activate_show_leds();
	}
	else if (show_leds == INACTIVE && show_leds_forced == INACTIVE)
	{
		deactivate_show_leds();
	}

	if (onboard_led == ACTIVE)
	{
		onboard_led_task15ms();

	}
}
