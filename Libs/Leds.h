/*
 * Leds.h
 *
 *  Created on: Dec 23, 2017
 *      Author: red
 */

#ifndef LEDS_H_
#define LEDS_H_

#define ALL_CHANNELS 0
#define RED 1
#define GREEN 2
#define BLUE 3
#define WHITE 4
#define PWM_CHANNELS 4u

#define GLOW_CYCLE 40u
#define STRIP_GLOW_STEP 25u
#define STRIP_PWM_MAX 1000u

#define STRIP_RAMP_FULL_ON_TIME 1u
#define STRIP_RAMP_FULL_ON_STEP 10u

typedef enum
{
	GLOW_UP,
	GLOW_ON,
	GLOW_DOWN,
	GLOW_OFF
} glowing;

extern void onboard_led_task15ms(void);
extern void led_strip_task15ms(void);
extern void activate_grow_leds(void);
extern void deactivate_grow_leds(void);
extern void activate_show_leds(void);
extern void deactivate_show_leds(void);
extern void glow(uint8_t glow_delay);
extern void strip_full_ramp_on_task_15ms(void);
extern void strip_full_ramp_off_task_15ms(uint8_t channel);
extern void strip_full_off(void);
extern uint8_t get_glow_color(void);


#endif /* LEDS_H_ */
