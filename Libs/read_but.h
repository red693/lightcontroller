/*
 * read_but.h
 *
 *  Created on: Sep 28, 2017
 *      Author: red
 */

#ifndef LIBS_READ_BUT_H_
#define LIBS_READ_BUT_H_

typedef enum
{
	SHORT_PRESS,
	LONG_PRESS,
	XL_PRESS,
	TREATED,
	MISSED,
	RELEASED,
	PRESSED,
	DEBOUNCING
} btn_state;

void read_buttons(void);

#define NR_BUTTONS 4u

#define GROW_BUTTON		0
#define SHOW_BUTTON		1
#define STRIP_BUTTON	2
#define FAN_BUTTON		3


#define SHORT_PRESS_T 50u
#define LONG_PRESS_T 147u
#define DEBOUNCE_TIME 4u

#endif /* LIBS_READ_BUT_H_ */
