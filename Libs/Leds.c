/*
 * Leds.c
 *
 *  Created on: Dec 23, 2017
 *      Author: red
 */


#include <stdio.h>
#include <stdlib.h>
#include "stm32f10x.h"
#include "AL_functions.h"
#include "local_defines.h"
#include "SoftwareTimer.h"
#include "Leds.h"

uint16_t led_blink_time = LED_OK_BLINK_TIME;
uint16_t timer_led, led_strip_main_timer;
uint16_t led_strip_main_time = LED_OK_BLINK_TIME;

int8_t all_on_current_step = 0u;
glowing glow_state = GLOW_UP;
uint8_t current_channel = RED;
uint16_t duty = 0u;

static void update_glow_pwm( uint8_t _steps, glowing _glow, uint8_t channel);

void onboard_led_task15ms(void)
{
	if (swTimer_isElapsed(timer_led))
	{
		swTimer_set(timer_led, led_blink_time);
		toggle_led();
	}
	else
	{
		swTimer_tick(timer_led);
	}
}


void led_strip_task15ms(void)
{
	static uint8_t pwm_step = 0;
	uint16_t duty;

	if (swTimer_isElapsed(led_strip_main_timer))
	{
		swTimer_set(led_strip_main_timer, led_strip_main_time);
		pwm_step++;
		if (pwm_step > 10)
			pwm_step = 0;
		duty = pwm_step * 1000 / 10;
		set_pwm_tim3(duty, 1);

	}
	else
	{
		swTimer_tick(led_strip_main_timer);
	}
}

void activate_grow_leds(void)
{
	GPIO_SetBits(GPIOA,GPIO_Pin_11);
}

void deactivate_grow_leds(void)
{
	GPIO_ResetBits(GPIOA,GPIO_Pin_11);
}

void activate_show_leds(void)
{
	GPIO_SetBits(GPIOA,GPIO_Pin_12);
}

void deactivate_show_leds(void)
{
	GPIO_ResetBits(GPIOA,GPIO_Pin_12);
}

void strip_full_ramp_on_task_15ms(void)
{
	//static uint16_t duty = 0u;

	if (swTimer_isElapsed(led_strip_main_timer))
	{
		swTimer_set(led_strip_main_timer, STRIP_RAMP_FULL_ON_TIME);
		if (duty < 1001u)
		{
			duty += STRIP_RAMP_FULL_ON_STEP;
			if (duty > 1001u)
				duty = 1001u;
			set_pwm_tim3(duty, RED);
			set_pwm_tim3(duty, GREEN);
			set_pwm_tim3(duty, BLUE);
			set_pwm_tim3(duty, WHITE);
		}
	}
	else
	{
		swTimer_tick(led_strip_main_timer);
	}
}

void strip_full_ramp_off_task_15ms(uint8_t channel)
{
	//static uint16_t duty = 0u;

	if (swTimer_isElapsed(led_strip_main_timer))
	{
		swTimer_set(led_strip_main_timer, STRIP_RAMP_FULL_ON_TIME);
		if (duty != 0)
		{
			duty -= STRIP_RAMP_FULL_ON_STEP;
			if (duty >0xFFE0)
				duty = 0u;
			if(channel == RED   || channel == ALL_CHANNELS) set_pwm_tim3(duty, RED);
			if(channel == GREEN || channel == ALL_CHANNELS) set_pwm_tim3(duty, GREEN);
			if(channel == BLUE  || channel == ALL_CHANNELS) set_pwm_tim3(duty, BLUE);
			if(channel == WHITE || channel == ALL_CHANNELS) set_pwm_tim3(duty, WHITE);
		}
	}
	else
	{
		swTimer_tick(led_strip_main_timer);
	}
}

void strip_full_off(void)
{
	uint16_t duty = 0u;
	set_pwm_tim3(duty, RED);
	set_pwm_tim3(duty, GREEN);
	set_pwm_tim3(duty, BLUE);
	set_pwm_tim3(duty, WHITE);
}

void glow(uint8_t glow_delay)
{
	static uint8_t step;
	if (swTimer_isElapsed(led_strip_main_timer))
	{
		swTimer_set(led_strip_main_timer, glow_delay);

		step++;
		if (step == GLOW_CYCLE)
		{
			glow_state++;
			if (glow_state > GLOW_OFF)
			{
				current_channel++;
				if(current_channel>PWM_CHANNELS) current_channel = 1u;
				glow_state = GLOW_UP;
			}
			step = 0;
		}
		update_glow_pwm(step, glow_state,current_channel);

	}
	else
	{
		swTimer_tick(led_strip_main_timer);
	}
}

static void update_glow_pwm( uint8_t _steps, glowing _glow, uint8_t channel)
{
//	uint16_t duty = 0u;
	if(_glow == GLOW_UP)
	{
		duty = _steps*STRIP_GLOW_STEP;
	}

	if (_glow == GLOW_ON && duty != STRIP_PWM_MAX)
	{
		duty = STRIP_PWM_MAX;
	}

	if(_glow == GLOW_DOWN)
	{
		duty = STRIP_PWM_MAX - _steps*STRIP_GLOW_STEP;
	}

	if(_glow == GLOW_OFF && duty!=0)
	{
		duty=0u;
	}
	set_pwm_tim3(duty, channel);
}

extern uint8_t get_glow_color(void)
{
	return current_channel;
}
