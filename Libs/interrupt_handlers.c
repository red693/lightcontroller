/*
 * interrupt_handlers.c
 *
 *  Created on: Aug 5, 2016
 *      Author: Andrei Rosu
 */

#include <stdio.h>
#include <stdlib.h>
#include "stm32f10x.h"
#include "AL_functions.h"
#include "stm32f10x_usart.h"
#include "stm32f10x_tim.h"
#include "stm32f10x_spi.h"
#include "local_defines.h"
#include "SoftwareTimer.h"



extern uint8_t time_out;

extern uint16_t ADCConvertedValue;

char rx_buffer[50];
char data;
char rx_buffer2[40];
char data2;
uint8_t rx_wr_index=0, rx_wr_index2=0;

void USART1_IRQHandler(void)
{

	if (USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
	{
		data = USART_ReceiveData(USART1);
		//if (data !='\r')
		if (data < 127u && data > 31u)
		{
			rx_buffer[rx_wr_index] = data;
			rx_wr_index++;
		}
		else
		{
//			if (rx_wr_index > 1 || rx_buffer[0] == '>')
//			{
				rx_buffer[rx_wr_index] = '\0';
				finished_command();
				rx_wr_index = 0;
//			}
		}

	}

}

//void USART2_IRQHandler(void)
//{
//
//	if(USART_GetITStatus(USART2, USART_IT_RXNE) != RESET)
//  {
//		data2=USART_ReceiveData(USART2);
//		if (data2 !=';')
//		{
//			rx_buffer2[rx_wr_index2]=data2;
//			rx_wr_index2++;
//		}
//		else
//		{
//			rx_buffer2[rx_wr_index2]='\0';
//			interpret_command2();
//			rx_wr_index2=0;
//		}
//  }
//
//}

void TIM2_IRQHandler(void)
{
   static uint16_t nr_timeouts = 0;

	TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
	nr_timeouts++;
	if (nr_timeouts >= 15)   //15ms
	{
		time_out = TRUE;
		nr_timeouts = 0;

	}
}

void ADC1_2_IRQHandler(void)
{
  /* Get injected channel13 converted value */
  ADCConvertedValue = ADC_GetConversionValue(ADC1);
}

//void EXTI11_IRQHandler(void)
//{
//	if (EXTI_GetITStatus(EXTI_Line11) != RESET)
//	{
//		EXTI_ClearITPendingBit(EXTI_Line11);
//		sprintf(sprintf_result, "External interrupt \n\r");
//		uart2_puts(sprintf_result);
//		//open_door = TRUE;
//		//swTimer_set(door_debouncer, DOOR_DEBOUNCE_TIME);
//	}
//
//}
