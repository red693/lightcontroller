/*
 * local_defines.h
 *
 *  Created on: Mar 16, 2017
 *      Author: ARosu
 */
#ifndef LOCAL_DEFINES_H_
#define LOCAL_DEFINES_H_

#define TRUE 1u
#define FALSE 0u

#define ON 1u
#define OFF 0u

#define CHECKBIT(ADDRESS,BIT) (ADDRESS & (1<<(BIT)))
#define SETBIT(ADDRESS,BIT_NO) (ADDRESS |= (1<<(BIT_NO)))
#define CLEARBIT(ADDRESS,BIT_NO) (ADDRESS &= ~(1<<(BIT_NO)))
#define TOGGLEBIT(ADDRESS,BIT_NO) (ADDRESS ^= (1<<(BIT_NO)))

#define LED_OK_BLINK_TIME 60u//650u
#define LED_ERROR_BLINK_TIME 15u
#define LED_CALLED_BLINK_TIME 1300u

#define CHECK_ALARM() !GPIO_ReadInputDataBit(GPIOB,GPIO_Pin_9)
#define FAN_ON() GPIO_SetBits(GPIOA,GPIO_Pin_8)
#define FAN_OFF() GPIO_ResetBits(GPIOA,GPIO_Pin_8)


typedef enum
{
	INACTIVE,
	ACTIVE
} led_state;

#endif /* LOCAL_DEFINES_H_ */
